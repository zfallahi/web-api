const config = {
  port: process.env.PORT || "3000",
  memgenCmd: "/home/user/memgen/memgen.sh",
}

export default config
