import base64
import http.client
import json

with open('example/chol.pdb', 'rb') as fp:
  connection = http.client.HTTPConnection('localhost:3000')

  connection.request('POST', '/api/submit', json.dumps({
    "pdb": str(base64.b64encode(fp.read()), 'ascii'),
    "apl": 0.65,
    "wpl": 40,
    "n": 64
  }), {'Content-type': 'application/json'})

  res = connection.getresponse()

  res_body = json.loads(res.read().decode())

  if res.status == 200:
    print("Output saved as [final.pdb] and [final.png]")

    with open('final.pdb', 'wb') as pdb_fp:
      pdb_fp.write(base64.b64decode(res_body["pdb"]))

    with open('final.png', 'wb') as png_fp:
      png_fp.write(base64.b64decode(res_body["png"]))
  else:
    print(f"Server response: {res_body['error']}")
    print("Saving error output as [out.log] and [err.log].")

    with open('out.log', 'w') as std_out_fp:
      std_out_fp.write(res_body["stdOut"])

    with open('err.log', 'w') as std_err_fp:
      std_err_fp.write(res_body["stdErr"])

    exit(1)
