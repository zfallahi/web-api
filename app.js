import bodyParser from "body-parser"
import { execaCommand } from 'execa'
import express from "express"
import fs from "fs"
import http from "http"
import _ from "lodash"
import path from "path"
import swaggerUi from "swagger-ui-dist"
import { fileURLToPath } from "url"

import config from "./config.js"
import memgen from './memgen-wrapper/memgen.js'
import tempExec from './memgen-wrapper/tempExec.js'

let app = express()
let server = http.createServer(app)

app.use(bodyParser.json())

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

app.use("/api", express.static(path.join(__dirname, "public")))
app.use("/api", express.static(swaggerUi.getAbsoluteFSPath()))

function decode(rawPdb) {
  if (rawPdb.toString('utf8').substr(0, 5) == "db://") {
    return rawPdb
  } else {
    return Buffer.from(rawPdb, "base64")
  }
}

app.post("/api/submit", async (req, res) => {
  const rawPdbs = req.body.pdbs
  const pdbs = _.map(rawPdbs, decode)

  const { ratio, areaPerLipid, waterPerLipid, lipidsPerMonolayer, addedSalt, boxShape, forceField } = req.body

  const result = await tempExec(memgen({ pdbs, ratio, areaPerLipid, waterPerLipid, lipidsPerMonolayer, addedSalt, boxShape, forceField }))

  if (result.success) {
    const { success, pdb, png, topology } = result

    res.json({ success,
      pdb: Buffer.from(pdb).toString('base64'),
      png: Buffer.from(png).toString('base64'),
      topology: Buffer.from(topology).toString('base64')
    })
  } else {
    res.statusCode = 500
    res.send(result)
  }
})

app.get("/api/structures", async (req, res) => {
  const result = await execaCommand(`memgen --run-locally --give-json --list-structures`, { shell: true })

  try {
    const structures = JSON.parse(result.stdout)

    res.json(structures)
  } catch (err) {
    res.statusCode = 500
    res.send(result)
  }
})

server.listen(config.port)

server.on("listening", () => {
  if (fs.existsSync('/workspaces')) {
    console.log()
    console.log(`Memgen REST API server listening on port ${config.port}`)
    console.log(" - visit http://localhost:3000/api for interactive documentation")
    console.log(" - call memgen with --server localhost:3000 to use local server")
    console.log()
  } else {
    console.log(`server listening on port ${config.port}, visit /api for documentation`)
  }
})
