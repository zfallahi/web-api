# Component map

```mermaid
graph TB
    py-memgen["memgen"]
    web-api["https://memgen.uni-saarland.de/api"]
    local-py-memgen["memgen --run-locally"]
    memgen[memgen.sh]

    py-memgen --> web-api
    web-api --> local-py-memgen
    local-py-memgen --> memgen
```

# Clone

This project includes submodules.

To clone, call following commands

```bash
$ git clone --recurse-submodules --jobs 2 git@gitlab.com:cbjh/memgen/web-api.git memgen-web-api
$ cd memgen-web-api
$ git submodule foreach -q --recursive 'branch="$(git config -f $toplevel/.gitmodules submodule.$name.branch || echo main)" ; git checkout $branch'
```

The last line is almost identical to `git submodule update --init --recursive --remote`, but it doesn't leave submodules in detached state.

# Container

- Open project in VSCode
- Install _Remote - Containers_ extension
- _View / Command Palette..._, _Remote-Containers: Rebuild and Reopen in Container_  

**Notes:**
- It will take about 10 minutes to build the container for the first time.
- When container opens, it automatically starts building custom packages and then starts local server. You can explore the code, but the server or the tests won't run correctly until the script finishes.
- It is possible to setup similar environment manually without a container, but it might be difficult and error-prone. Use of this container is recommended as it provides consistent development, testing and deployment environment.

# Development

## Dependencies

Dependencies should be already installed when container opens.

Otherwise the commands are


```shell
$ ./memgen/packages/install.sh
$ npm ci --include dev
```

# Run

Server should be already running.

It can be stopped by pressing ctrl+c, then run again with

```bash
$ npm run dev
```

## Test

Use panels offered by VSCode, otherwise call

```shell
$ npm run test
```

**Note:** Python tests do not appear in the test panel until you open `memgen/test_memgen.py`.

# Production

## Dependencies

```shell
$ npm ci
```

## Run

```shell
$ npm run serve
```
